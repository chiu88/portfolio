const projects = document.querySelectorAll('.project');

function toggleOpen() {
  console.log('Hello');
  this.classList.toggle('open');
}

function toggleActive(e) {
  console.log(e.propertyName);
  if (e.propertyName.includes('flex')) {
    this.classList.toggle('open-active');
  }
}

projects.forEach(project => project.addEventListener('click', toggleOpen));
projects.forEach(project => project.addEventListener('transitionend', toggleActive));

// jquery for fade in hero text

$(function() {

  setTimeout(function() {
    $('.hero-text').removeClass('hidden');

  });

});
