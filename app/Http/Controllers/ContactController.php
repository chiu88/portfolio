<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

/**
 *
 */
class ContactController extends Controller
{

    function create() {



        return view('contact');
    }

    function store() {
        $request = request();
        $result = $request->validate(
            [ 'firstName' => 'required|max:255',
             'lastName' => 'required|max:255',
             'email' => 'required|string|email|max:255' ]
        );

        $data = request()->all();
        $contact = new Contact();
        $contact->first_name = $data['firstName'];
        $contact->last_name = $data['lastName'];
        $contact->email = $data['email'];
        $contact->save();

        return redirect('/contact')->with('message', 'Thank you for your interest, please allow 3-5 days for me to get back to you');
    }

}


 ?>
