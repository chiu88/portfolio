@extends('layout-page')

@section('content')




  <div class="contact-body-section">
    <h1>Contact</h1>

    <div class="contact-section flex flex-horizontal">
      <div class="iframe-container">
        <p>Peter Shawn Chiu</p>
        <p>Location: Calgary, Alberta</p>
        <p>Email: <a href="mailto:shawn.chiu88@live.com">shawn.chiu88@live.com</a></p>

        <iframe width="250" height="250" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJ1T-EnwNwcVMROrZStrE7bSY&key=AIzaSyDSAJ3tvkrDws3LIr71ogt5WejhS5tlUFU" allowfullscreen></iframe>
      </div>

      <div class="contact-form-container">
        <p>If you would like to work together, send me your contact info, I will get back to you in a timely manner</p>


        <form method="post">
          {{csrf_field()}}

          @include('forms.text', [
          'name' => 'firstName',
          'label' => 'First Name:'
          ])

          @include('forms.text', [
          'name' => 'lastName',
          'label' => 'Last Name:'
          ])

          @include('forms.text', [
          'name' => 'email',
          'label' => 'Email:'
          ])


          <input type="submit" name="" value="Submit" class="submit">

        </form>


      </div>
    </div>

  </div>



@endsection
