
<div class="flex flex-vertical">
    {{$label}} <br>
    <input type="text" name="{{$name}}" value="{{old($name)}}" class='form-control <?php echo $errors->has($name) ? 'is-invalid' : ''; ?>'>
    <?php if($errors->has($name)): ?>
        <span class='invalid-feedback'> {{$errors->first($name)}}</span>
    <?php endif; ?>
    <br>
</div>
