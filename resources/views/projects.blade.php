@extends('layout-page')

@section('content')

<section class='projects-section-container page'>
  <h1>My Work</h1>
  <br>
  <br>
  <br>
  <div class="projects">
    <div class="project-1 project" title="javascript">
      <p></p>
      <div class="project-icon-1 project-icon" title="javascript"></div>
      <p>Rock Paper Scissors Dynamite</p>
      <p>Click to expand</p>
      <a href="https://s3.ca-central-1.amazonaws.com/rock-paper-scissors-dynamite/rock-paper-scissors/index.html" class='project-link-btn'>Visit Site</a>

    </div>

      <div class="project-2 project" title="php">
        <p></p>
        <div class="project-icon-2 project-icon"></div>
        <p>Google News</p>
        <p>Click to expand</p>
        <a href="http://35.183.39.129" class='project-link-btn'>Visit Site</a>
      </div>


      <div class="project-3 project" title="wordpress">
        <p></p>
        <div class="project-icon"></div>
        <p>Schelt Visitor Center</p>
        <p>Click to expand</p>
        <a href="http://162.222.178.6/" class='project-link-btn'>Visit Site</a>

      </div>


  </div>

  <div class="projects">
    <div class="project-4 project" title="javascript">
      <p></p>
      <div class="project-icon-1 project-icon"></div>
      <p>Shopping Cart</p>
      <p>Click to expand</p>
      <a href="https://s3.ca-central-1.amazonaws.com/cart-shopping-shawn/Shopping+Cart/index.html" class='project-link-btn'>Visit Site</a>
    </div>
    <div class="project-5 project" title="html-css">
      <p></p>
      <div class="project-icon-4 project-icon"></div>
      <p>HTML/CSS</p>
      <p>Click to expand</p>
      <a href="https://s3.ca-central-1.amazonaws.com/peter-chiu-assign4/CPNT260-assign4/index.html" class='project-link-btn'>Visit Site</a>
    </div>

  </div>

</section>

@endsection
