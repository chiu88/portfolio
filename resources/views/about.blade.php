@extends('layout-page')

@section('content')

<section class="about-section flex flex-horizontal">
  <div class="profile-picture" title="shawn chiu">

  </div>
  <div class="about-text-container" id='about-text-container'>
    <h2>About Me</h2>
    <p>I am a Front-End, and Wordpress developer.
    I am a graduate of SAIT with a 'Web Developer' certificate. I was an electrician and a general contractor doing anything from commercial electrical work to home renovations.
    I started pursuing a new career and fell in love with web development.
    </p>
    <p></p>
    <div class="skills">
      <h2>Skills</h2>
      <div class="skills-description-container">
        <div class="skills-description">
          HTML
        </div>
        <div class="skills-description">
          CSS
        </div>
        <div class="skills-description">
          JavaScript
        </div>
        <div class="skills-description">
          PHP(Laravel)
        </div>
        <div class="skills-description">
          Wordpress
        </div>
        <div class="skills-description">
          jQuery
        </div>
        <div class="skills-description">
          MySQL
        </div>
        <div class="skills-description">
          C#
        </div>
        <div class="skills-description">
          Java
        </div>

      </div>
    </div>
    <br>
    <br>
    <p>Currently taking on new projects:</p>
    <a href="/contact"><p>Contact me now</p></a>

  </div>
</section>

@endsection
