<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="description" content="Portfolio of Peter Shawn Chiu Web Developer HTML CSS PHP Javascript">
  <meta name='viewport' content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="/css/app.css">
  <title>Shawn Chiu</title>

</head>



<header class="flex flex-horizontal header-page">
  <a href="/" class='nav-link-btn'>Home</a>
  <a href="/about" class='nav-link-btn'>About</a>
  <a href="/projects" class='nav-link-btn'>My Work</a>
  <a href="/contact" class='nav-link-btn'>Contact</a>
</header>

<!-- This will show that the data was input successfully and stored -->
<div class="alert-container">

  <?php if($message = session('message')): ?>
    <div class="alert alert-success">
      {{$message}}
    </div>
  <?php endif; ?>
</div>

<body class="background-image">
  @yield('content')

  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script type="text/javascript" src="/js/script.js"></script>

</body>

</html>
<footer class='flex flex-horizontal justify-center'>
  <ul>
    <li>Shawn Chiu</li>
    <li>Calgary, Alberta</li>
    <li>
      <a href="mailto:shawn.chiu88@live.com">shawn.chiu88@live.com</a>
    </li>
  </ul>


</footer>
